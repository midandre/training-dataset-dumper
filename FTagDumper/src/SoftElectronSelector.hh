#ifndef SOFT_ELECTRON_SELECTOR_HH
#define SOFT_ELECTRON_SELECTOR_HH

#include "xAODBase/IParticleContainer.h"
#include "xAODEgamma/ElectronContainerFwd.h"
#include "xAODJet/JetFwd.h"

#include "SoftElectronSelectorConfig.hh"

class SoftElectronSelector
{
public:
  SoftElectronSelector(SoftElectronSelectorConfig = SoftElectronSelectorConfig(),
                       const std::string& link_name = "GhostElectrons");

  typedef std::vector<const xAOD::Electron*> Electrons;
  Electrons get_electrons(const xAOD::Jet& jet) const;

private:
  typedef SG::AuxElement AE;
  typedef std::vector<ElementLink<xAOD::IParticleContainer> > PartLinks;
  std::function<Electrons(const AE&)> m_electron_associator;

  bool passed_cuts(const xAOD::Jet& jet, const xAOD::Electron &el) const;
  SoftElectronSelectorConfig::Cuts m_electron_select_cfg;
};

#endif
