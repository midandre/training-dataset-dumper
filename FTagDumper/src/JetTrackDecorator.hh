#ifndef TRACK_DECORATOR_HH
#define TRACK_DECORATOR_HH

#include "xAODJet/JetContainerFwd.h"
#include "xAODTracking/TrackParticle.h"

class JetTrackDecorator {
 public:
  using AE = SG::AuxElement;

  JetTrackDecorator(const std::string& suffix="",
		    const std::string& writer_name="tracks");

  void decorate(const xAOD::Jet& jet,
		const std::vector<const xAOD::TrackParticle*>& tracks) const;

 private:
  AE::ConstAccessor<int> m_ftagTruthOriginLabel;

  AE::Decorator<int> m_n_tracks;
  AE::Decorator<float> m_PU_track_fraction;

};
#endif



  

