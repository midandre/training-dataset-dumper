#ifndef TRACK_SUBJETS_DECORATOR_HH
#define TRACK_SUBJETS_DECORATOR_HH

#include "xAODJet/JetFwd.h"
#include "xAODJet/JetContainerFwd.h"
#include "xAODTracking/TrackParticleContainerFwd.h"

#include "FlavorTagDiscriminants/BTagTrackIpAccessor.h"

#include "JetDumperConfig.hh"


class TrackSubjetsDecorator
{
public:
  TrackSubjetsDecorator(SubjetConfig config,
                        const std::string& link_name = "BTagTrackToJetAssociator");
  

  void decorate(const xAOD::Jet* jet, std::vector<const xAOD::Jet*> subjets) const;
private:
  typedef SG::AuxElement AE;
  typedef std::vector<const xAOD::IParticle*> Particles;

  typedef std::vector<ElementLink<xAOD::TrackParticleContainer> > TrackLinks;
  typedef std::vector<ElementLink<xAOD::IParticleContainer>> ParticleLinks;
  typedef ElementLink<xAOD::JetContainer> JetLink;

  std::function<Particles(const AE&)> m_jet_track_associator;
  std::function<Particles(const AE&)> m_subjet_track_associator;
  AE::Decorator<int> m_track_subjet_index;

  std::vector<const xAOD::Jet*> getSubjets(const xAOD::Jet* jet) const;

};

#endif
