#include "TrackSubjetsDecorator.hh"
#include "xAODBTagging/BTaggingUtilities.h"

TrackSubjetsDecorator::TrackSubjetsDecorator(SubjetConfig config, const std::string& link_name):
  m_jet_track_associator(nullptr),
  m_subjet_track_associator(nullptr),
  m_track_subjet_index("subjetIndex")
{
  AE::ConstAccessor<ElementLink<xAOD::BTaggingContainer>> btag_link(
      config.btagging_link);
  AE::ConstAccessor<ParticleLinks> jet_acc(link_name);
  AE::ConstAccessor<TrackLinks> subjet_acc("BTagTrackToJetAssociator");
  m_jet_track_associator = [jet_acc](const AE& jet) -> Particles {
    Particles particles;
    for (const auto& link: jet_acc(jet)) {
      if (!link.isValid()) {
        throw std::logic_error("invalid particle link in TrackSubjetsDecorator, at large-R jet");
      }
      particles.push_back(*link);
    }
    return particles;
  };
  m_subjet_track_associator = [subjet_acc, btag_link](const AE& jet) -> Particles {
    Particles particles;
    const xAOD::BTagging* btag = *btag_link(jet);
    if (!btag) {
      throw std::logic_error("missing btaggingLink in TrackSubjetsDecorator");
    }
    for (const auto& link: subjet_acc(*btag)) {
      if (!link.isValid()) {
        throw std::logic_error("invalid track link in TrackSubjetsDecorator");
      }
      particles.push_back(*link);
    }
    return particles;
  };
}

// this call actually does the work on the jet
void TrackSubjetsDecorator::decorate(const xAOD::Jet* jet, std::vector<const xAOD::Jet*> subjets) const {
  for (const auto& tp: m_jet_track_associator(*jet)){
    m_track_subjet_index(*tp) = -2;
  }
  for (std::size_t index = 0; index < subjets.size(); index++){
    const xAOD::Jet* subjet = subjets.at(index);
    for (const auto& tp: m_subjet_track_associator(*subjet)){
      m_track_subjet_index(*tp) = index;
    }
  }
}