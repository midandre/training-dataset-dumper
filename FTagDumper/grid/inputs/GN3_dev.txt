# Single b-tag training samples for GN3
# https://ftag.docs.cern.ch/software/samples/
# For these trainings we will avoid the standard ttbar
# so that we don't have to k-fold the model.

# -------------------------------------------------------------------------------------------------
# Dedicated ttbar training samples: mc20d, mc23a and mc23c (https://its.cern.ch/jira/browse/ATLFTAGDPD-395)
# -------------------------------------------------------------------------------------------------
mc20_13TeV.601589.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.deriv.DAOD_FTAG1.e8547_s3797_r13144_p6368
mc23_13p6TeV.601589.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.deriv.DAOD_FTAG1.e8549_s4159_r14799_p6368
mc23_13p6TeV.601589.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.deriv.DAOD_FTAG1.e8549_s4162_r14622_p6368

# ------------------------------------
# Default run 3 training samples: mc23a
# ------------------------------------
# Z' extended
mc23_13p6TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e8514_s4162_r14622_p6368

# -------------------------------------
# Default run 2 training samples: mc20d
# -------------------------------------
# Z' extended
mc20_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e7954_s3797_r13144_p6368
mc20_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e7954_s3681_r13144_p6368
