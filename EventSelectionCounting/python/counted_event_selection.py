from AnalysisAlgorithmsConfig.ConfigSequence import ConfigSequence
from AnalysisAlgorithmsConfig.ConfigBlock import ConfigBlock
from EventSelectionAlgorithms.EventSelectionConfig import (
    EventSelectionMergerConfig, EventSelectionConfig
)
from typing import Literal, Optional


class EventCutFlowBlock(ConfigBlock):
    def __init__(
        self,
        selection_id: str,
        counts_output_path: str,
        count_total_before_selection: bool
    ):
        super(EventCutFlowBlock, self).__init__()
        self.selection_id = selection_id
        self.counts_output_path = counts_output_path
        self.count_total_before_selection = count_total_before_selection

    def makeAlgs(self, config):
        algorithm = config.createAlgorithm(
            "EventSelectionCounterAlg", f"EventSelectionCounterAlg_{self.selection_id}"
        )
        algorithm.eventInfo = config.readName("EventInfo")
        algorithm.selections = [
            f"{selection},as_char"
            for selection in config.getEventCutFlow(self.selection_id)
        ]
        algorithm.countsOutputPath = self.counts_output_path
        algorithm.includeTotalCount = self.count_total_before_selection


def configure_counted_event_selection(
    seq: ConfigSequence,
    selection_id: str,
    selections: list[str],
    electrons: Optional[str] = None,
    muons: Optional[str] = None,
    jets: Optional[str] = None,
    large_r_jets: Optional[str] = None,
    photons: Optional[str] = None,
    taus: Optional[str] = None,
    met: Optional[str] = None,
    met_term_definition: Literal["Final", "NonInt"] = "Final",
    btag_decoration: Optional[str] = None,
    filter_events: bool = True,
    cutflow_counts_output_path: Optional[str] = None,
    count_total_before_selection: bool = True,
):
    """
    Add event selection algorithm configuration to the configuration sequence.
    For information on the format of possible selections, consult the
    [TopCPToolkit docs](https://topcptoolkit.docs.cern.ch/settings/eventselection/).
    One selection cut line correponds to one list item here and there is no need to add
    "SAVE" at the end of the selections, as this is done automatically (and will result
    in an error when attempted manually).
    Container names are only required to be specified if they are used in a selection.

    Args:
        seq: The configuration sequence to add the event selection to
        selection_id: A unique identifier for this event selection
        selections: The list of selections to apply
        electrons: The electron input container name to use
        muons: The muon input container name to use
        jets: The jet input container to use
        large_r_jets: The large-R jet input container name to use
        photons: The photon input container name to use
        taus: The tau input container name to use
        met: The missing ET input container to use
        met_term_definition: The MET term definition to use
        btag_decoration: The b-tagging decoration name to use
        filter_events: wether to drop events in subsequent processing based on the
            selection or only decorate them
        cutflow_counts_output_path: when specified cutflow counts will be saved under
            this path
        count_total_before_selection: if saving cutflow counts, wether to also save the
            total number of events before the selection
    """

    assert met_term_definition in ["Final", "NonInt"]

    # we need to add SAVE at the end of the selections, otherwise nothing will happen;
    # we do that automatically here, so the user can't forget it; this is a friendly
    # reminder that we do this and it doesn't have to be done manually
    assert not any("SAVE" in selection for selection in selections)

    selection_config = EventSelectionConfig(selection_id)
    selection_config.setOptionValue("selectionCuts", "\n".join(selections) + "\nSAVE\n")

    selection_config.setOptionValue("electrons", electrons)
    selection_config.setOptionValue("muons", muons)
    selection_config.setOptionValue("jets", jets)
    selection_config.setOptionValue("largeRjets", large_r_jets)
    selection_config.setOptionValue("photons", photons)
    selection_config.setOptionValue("taus", taus)
    selection_config.setOptionValue("met", met)
    selection_config.setOptionValue("btagDecoration", btag_decoration)

    selection_config.setOptionValue("metTerm", met_term_definition)

    # don't support preselection for now, until we explicitly need it
    selection_config.setOptionValue("preselection", "")

    # don't filter here in case we need all events for cutflow counts
    selection_config.setOptionValue("noFilter", True)

    # we don't have a root output, so don't allow the debug mode
    selection_config.setOptionValue("debugMode", False)

    seq.append(selection_config)

    # add event cutflow algorithm
    if cutflow_counts_output_path is not None:
        seq.append(
            EventCutFlowBlock(
                selection_id, cutflow_counts_output_path, count_total_before_selection
            )
        )

    # this only applies the actual filter to the events
    selection_merger_config = EventSelectionMergerConfig()
    selection_merger_config.setOptionValue("selections", [f"pass_{selection_id}_%SYS%"])
    selection_merger_config.setOptionValue("noFilter", not filter_events)
    seq.append(selection_merger_config)
